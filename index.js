let expElement = document.getElementById("experience");
let studElement = document.getElementById("studies");
let printBtn = document.getElementById("printBtn");

function verExperiencia() {
  $(expElement).slideToggle("fast");
  $(studElement).slideUp("fast");
}

function verEstudios() {
  $(studElement).slideToggle("fast");
  $(expElement).slideUp("fast");
}

printBtn.addEventListener('click', () => {
  var myName = document.getElementById("name").innerText;
  for (let i = 0; i < 100; i++) {
    console.log(myName);
  };
})
